import https from 'node:https'
import AutoEncryptLocalhost from '../../index.js'

const keyMaterial = AutoEncryptLocalhost.getKeyMaterial()

const server = https.createServer(keyMaterial, (request, response) => {
  response.end('Hello, world!')
})

server.listen(443, () => {
  console.log('Web server is running at https://localhost')
})
