// Just get the key material (can be used to create a CLI app, for example.)

import AutoEncryptLocalhost from '../../index.js'

const keyMaterial = AutoEncryptLocalhost.getKeyMaterial()

// And keyMaterial.key holds the private key.
// The Certificate Authority and all certificate PEMs can be found at
// the default settings path which is
// <DATA HOME>/.local/share/small-tech.org/auto-encrypt-localhost/

console.log(keyMaterial.cert)
