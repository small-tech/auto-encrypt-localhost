// Create an https server using locally-trusted certificates.

import AutoEncryptLocalhost from '../../index.js'

// Forward all status events to their corresponding console level log.
const events = AutoEncryptLocalhost.events
events.onAll(events.INFORMATION, console.info)
events.onAll(events.WARNINGS, console.warn)
events.onAll(events.ERRORS, console.error)

const server = AutoEncryptLocalhost.https.createServer((request, response) => {
  response.end('Hello, world!')
})

server.listen(443, () => {
  console.info('\n🎉 Web server is running at https://localhost\n')
})
