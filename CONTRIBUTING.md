Thank you for your interest in improving Auto Encrypt Localhost.

To make sure your effort isn’t wasted, please:

1. [Open an issue](https://codeberg.org/small-tech/auto-encrypt-localhost) outlining the changes you want to make.

2. _Once the changes have been agreed on_, please prepare and [submit a pull request](https://codeberg.org/small-tech/auto-encrypt-localhost/pulls) implementing the changes.

When preparing a pull request, please:

  - Keep changes to a minimum.
  - Do not change the coding style or make large refactorings unrelated to the change at hand.

Thank you again.

[Aral](https://ar.al)
