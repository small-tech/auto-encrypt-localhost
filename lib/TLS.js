/**
 * TLS: Creates certificate authorities and TLS certificates. 
 *
 * @module @small-tech/auto-encrypt-localhost
 * @copyright © 2019-present Aral Balkan, Small Technology Foundation.
 * @license AGPLv3.
 */

import CertificateAuthority from './CertificateAuthority.js'
import ServerCertificate from './ServerCertificate.js'

/**
 * Creates (if necessary) and returns the key material for setting up a TLS server.
 *
 * @param settingsPath {string} A custom settings path.
 *
 * @typedef {Object} KeyMaterial
 * @property {string} cert                               - Certificate (public) in PEM format.
 * @property {string} key                                - Private key in PEM format.
 * @property {CertificateAuthority} certificateAuthority - Certificate authority.
 *
 * @returns {KeyMaterial} Object with the certificate and private keys in PEM format.
 */
export function getKeyMaterial (settingsPath) {
  const certificateAuthority = new CertificateAuthority(settingsPath)
  const serverCertificate = new ServerCertificate(settingsPath, certificateAuthority)

  return serverCertificate.keyMaterial
}
