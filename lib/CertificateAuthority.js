/**
 * Certificate Authority
 * See RFC 1422 (§3.3)
 * https://www.rfc-editor.org/rfc/rfc1422
 */

import os from 'node:os'
import fs from 'node:fs'
import path from 'node:path'
import { execSync } from 'node:child_process'

import syswideCas from '@small-tech/syswide-cas'

import events from './Events.js'
import AbstractCertificate from './AbstractCertificate.js'

const platform = {
  is: function (platformToCheck) { return this[platformToCheck] === this.current },
  current: os.platform(),
  LINUX: 'linux',
  MAC: 'darwin',
  WINDOWS: 'win32'
}

export default class CertificateAuthority extends AbstractCertificate {
  constructor (settingsPath) {
    super(settingsPath)

    this.privateKeyBaseFileName = 'auto-encrypt-localhost-CA-key'
    this.certificateBaseFileName = 'auto-encrypt-localhost-CA'

    const actionTaken = this.initialise()
    if (actionTaken === this.CREATED_KEY) {
      this.addToPersistedTrustStores()
    }
    // The Node trust store is for this session only so we must add
    // the Certificate Authority (CA) to it every time, not just when
    // the CA is created like the system and Firefox trust stores.
    this.addToNodeTrustStore()
  }

  get certificateFileNameWithSerialNumber () {
    return this.fileNameWithSerialNumberFor(this.certificateBaseFileName)
  }

  /**
   * @param fileName {string} The file name (sans extension) to get the full file name for.
   * @returns {string} File name with serial number and extension for use when writing to system trust store.
   */
  fileNameWithSerialNumberFor (baseFileName) {
    return `${baseFileName}-${this.certificate.serialNumber}.pem`
  }

  addToPersistedTrustStores () {
    this.addToSystemTrustStore()
    this.addToFirefoxTrustStore()
  }

  addToNodeTrustStore () {
    // Add root store to Node to ensure Node recognises the certificates
    // (e.g., when using https.get(), etc.)
    syswideCas.addCAs(this.certificateFilePath)
  }

  addToSystemTrustStore () {
    switch (platform.current) {
      case platform.LINUX: this.addToLinuxSystemTrustStore(); break
      case platform.MAC: this.addToDarwinSystemTrustStore(); break
      case platform.WINDOWS: this.addToWindowsSystemTrustStore(); break
      default:
        // This should never be reached as we should be failing early
        // on unsupported platforms.
        throw new Error(`Unsupported platform: ${platform.current}`)
    }
  }

  addToLinuxSystemTrustStore () {
    // Based on how mkcert does it at
    // https://github.com/FiloSottile/mkcert/blob/2a46726cebac0ff4e1f133d90b4e4c42f1edf44a/truststore_linux.go#L27
    let systemTrustCertificateDirectory
    let systemTrustStoreUpdateCommand

    switch (true) {
      case fs.existsSync('/etc/pki/ca-trust/source/anchors/'):
        systemTrustCertificateDirectory = '/etc/pki/ca-trust/source/anchors/'
        systemTrustStoreUpdateCommand = 'update-ca-trust extract'
      break

      case fs.existsSync('/usr/local/share/ca-certificates/'):
        systemTrustCertificateDirectory = '/usr/local/share/ca-certificates/'
        systemTrustStoreUpdateCommand = 'update-ca-certificates'
      break

      case fs.existsSync('/etc/ca-certificates/trust-source/anchors/'):
        systemTrustCertificateDirectory = '/etc/ca-certificates/trust-source/anchors/'
        systemTrustStoreUpdateCommand = 'trust extract-compat'
      break

      case fs.existsSync('/usr/share/pki/trust/anchors'):
        systemTrustCertificateDirectory = '/usr/share/pki/trust/anchors/'
        systemTrustStoreUpdateCommand = 'update-ca-certificates'
      break

      default:
        events.emit(events.warnings.UNSUPPORTED_LINUX_SYSTEM_STORE, `Installing certificate authories to the system store is not supported on this Linux distribution yet.\n\nIf you want to help add support, please open an issue at:\nhttps://codeberg.org/small-tech/auto-encrypt-localhost/issues\n\nPlease find the certificate authority at the following path and add it to your system store manually:\n${this.certificateFilePath}\n\nThank you.`)
    }

    // Copy the certificate authority to the system trust store location
    // and issue the command to update the system trust store.
    this.certificatePathForSystemTrustStore = path.join(systemTrustCertificateDirectory, this.certificateFileNameWithSerialNumber)

    execSync(`sudo cp ${this.certificateFilePath} ${this.certificatePathForSystemTrustStore}`)
    execSync(`sudo ${systemTrustStoreUpdateCommand}`)
  }

  addToDarwinSystemTrustStore () {
    this.certificatePathForSystemTrustStore = this.certificateFilePath
    execSync(`sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ${this.certificatePathForSystemTrustStore}`)
  }

  powerShellCommandWithElevatedPrivileges (command) {
    return `Powershell Start-Process -Verb RunAs powershell.exe -Args '${command}'`
  }

  safeDirectoryPathForWindows (directoryPath) {
    return  execSync(`Powershell (New-Object -ComObject Scripting.FileSystemObject).GetFolder('${directoryPath}').ShortPath`).toString().replace('\r\n', '')
  }

  addToWindowsSystemTrustStore () {
    // Because Windows is hacks all the way down, the only way I’ve been able to get Import-Certificate
    // to work if there is a space in the path (e.g., if your local account has a space in it,
    // like C:\Users\Aral Balkan\…) is to use a PowerShell command to ensure we use shortnames
    // (remember those from the DOS days?) in the path.
    const safeSettingsPathForWindows = this.safeDirectoryPathForWindows(this.settingsPath)
    const safeCertificateFilePathForWindows = path.join(safeSettingsPathForWindows, this.certificateFileName) 
  
    this.certificatePathForSystemTrustStore = safeCertificateFilePathForWindows

    // Making sure we start a new administrative process so that this works regardless of whether a normal
    // or elevated privilege Windows Terminal session is in progress.
    execSync(this.powerShellCommandWithElevatedPrivileges(`Import-Certificate -FilePath ${this.certificatePathForSystemTrustStore} -CertStoreLocation Cert:\\LocalMachine\\Root`))
  }

  addToFirefoxTrustStore () {
    let firefoxPath
    let firefoxPoliciesDirectoryPath
    let policy
    let policies
    let applyPlatformSpecificTransformationsToCommand

    const temporaryPoliciesFilePath = path.join(os.tmpdir(), 'policies.json')

    // On Mac and Windows, if we activate ImportEnterpriseRoots, custom
    // certificates are automatically imported from the system trust store.
    // In other words, which this flag set, they behave like Chrom(ium)
    // does without requiring any configuration.
    const MAC_AND_WINDOWS_POLICY = `"Certificates": {"ImportEnterpriseRoots": true}`
    
    // On Linux, Firefox behaves differently. We have to create a
    // policy file and include the path to the certificate authority in it.
    const LINUX_POLICY = `"Certificates": {"Install": [${this.certificateFilePath}]}`

    const policiesWithPolicy = policy => `{"policies": {${policy}}}`

    switch (platform.current) {
      case platform.LINUX:
        policy = LINUX_POLICY
        applyPlatformSpecificTransformationsToCommand = command => `sudo ${command}`
        firefoxPath = null // We don’t install to Firefox’s path on Linux. And there’s no harm in creating this even if Firefox is not installed.
        firefoxPoliciesDirectoryPath = '/etc/firefox/policies/'
      break

      case platform.MAC:
        policy = MAC_AND_WINDOWS_POLICY
        applyPlatformSpecificTransformationsToCommand = command => command // No platform-specific transformations necessary on macOS.    
        firefoxPath = path.join('/', 'Applications', 'Firefox.app')
        firefoxPoliciesDirectoryPath = path.join(firefoxPath, 'Contents', 'Resources', 'distribution')
      break

      case platform.WINDOWS:
        policy = MAC_AND_WINDOWS_POLICY
        applyPlatformSpecificTransformationsToCommand = command => this.powerShellCommandWithElevatedPrivileges(command)
        firefoxPath = path.join('C:', 'Program Files', 'Mozilla Firefox')

        let safeFirefoxPathForWindows
        try {
          safeFirefoxPathForWindows = this.safeDirectoryPathForWindows(firefoxPath)
        } catch (error) {
          // Could not get safe directory path for windows.
          // Likely because Firefox is not installed. Ignore this for now, we’ll
          // flag it later on when we check for exactly this scenario.
          safeFirefoxPathForWindows = firefoxPath
        }
        firefoxPoliciesDirectoryPath = path.join(safeFirefoxPathForWindows, 'distribution')
      break

      default:
        throw new Error(`Unsupported platform: ${platform.current}`)
    }

    this.firefoxPoliciesFilePath = path.join(firefoxPoliciesDirectoryPath, 'policies.json')

    const firefoxWarningWithManualSetupHelpMessage = (showPath = true) =>
      `If you get a certificate error in Firefox, please manually set security.enterprise_roots.enabled to true in about:config or add the following policy rule to your policies.json file ${showPath ? `at ${this.firefoxPoliciesFilePath}` : ''}:\n\n${policy}`

    if (firefoxPath === null || fs.existsSync(firefoxPath)) {
      // Either Firefox is installed or we’re on Linux where we can install
      // the global policy anyway and it’ll work even if Firefox is installed later.
      policies = policiesWithPolicy(policy)

      if (fs.existsSync(this.firefoxPoliciesFilePath)) {
        // Do not overwrite existing policy files.
        // (We don’t want to accidentally mess up someone’s system.)
        events.emit(events.warnings.FIREFOX_POLICY_FILE_ALREADY_EXISTS, `A policy file already exists at ${this.firefoxPoliciesFilePath}, Auto Encrypt Localhost will not overwrite it.\n\n${firefoxWarningWithManualSetupHelpMessage(/* showPath */ false)}`)
      } else {
        // Create a policy file for Firefox to make it recognise our development certificate.
        try {
          fs.writeFileSync(temporaryPoliciesFilePath, policies, 'utf-8')
          execSync(applyPlatformSpecificTransformationsToCommand(`mkdir -p ${firefoxPoliciesDirectoryPath}`))
          execSync(applyPlatformSpecificTransformationsToCommand(`mv ${temporaryPoliciesFilePath} ${firefoxPoliciesDirectoryPath}`))
        } catch (error) {
          events.emit(events.errors.CREATING_FIREFOX_POLICY_FILE, `Error while attempting to create Firefox policy file:\n\n${error}\n\n${firefoxWarningWithManualSetupHelpMessage()}`)
        }
      }
    } else {
      // We’re not on Linux and Firefox is not installed.
      events.emit(events.warnings.FIREFOX_NOT_FOUND_IN_DEFAULT_LOCATION, `Firefox could not be found in the default location.\n\n${firefoxWarningWithManualSetupHelpMessage(/* showPath */ false)}`)
    }
  }

  finishCreatingKeyMaterial () {
    this.certificate.privateKey = this.privateKey
    this.validity = 100 // years
 
    const attributes = [
      {
        shortName: 'O',
        value: 'Auto Encrypt Localhost'
      },
      {
        shortName: 'OU',
        value: 'https://codeberg.org/small-tech/auto-encrypt-localhost'
      },
      {
        shortName: 'CN',
        value: `Localhost Certificate Authority for ${this.accountAtHost}`
      }
    ]

    const extensions = [
      { name: 'basicConstraints', cA: true },
      { name: 'keyUsage', keyCertSign: true }
    ]

    this.certificate.setSubject(attributes)
    this.certificate.setIssuer(attributes)
    this.certificate.setExtensions(extensions)

    this.signCertificate(this.privateKey)
    this.persistKeyMaterial()
  }
}
