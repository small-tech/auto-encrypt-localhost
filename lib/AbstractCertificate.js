/**
 * Abstract base class common to server and certificate authority certificates.
 */

import os from 'node:os'
import fs from 'node:fs'
import path from 'node:path'

import forge from 'node-forge'

import events from './Events.js'

export default class AbstractCertificate {
  certificate
  certificatePem
  certificateBaseFileName

  privateKey
  privateKeyPem
  privateKeyBaseFileName

  LOADED_KEY = 'loaded key'
  CREATED_KEY = 'created key'

  hostName = os.hostname()
  accountName = os.userInfo().username
  accountAtHost = `${this.accountName} at ${this.hostName}`
  
  // Subject Alternative Name types.
  // https://www.rfc-editor.org/rfc/rfc5280#section-4.2.1.6
  DNS_TYPE = 2
  IP_TYPE = 7

  /**
   * @param settingsPath {String} Path to save the certificates to.
   */
  constructor (settingsPath) {
    if (settingsPath == undefined) {
      throw new Error('Missing argument: settingsPath')
    }
    this.settingsPath = settingsPath

    // Ensure settings path exists.
    fs.mkdirSync(this.settingsPath, { recursive: true })
  }

  /**
   * @typedef {Object} KeyMaterial
   * @property {string} The certificate (public) in PEM format.
   * @property {string} The private key in PEM format.
   *
   * @returns {KeyMaterial} Object with the certificate and private keys in PEM format.
   */
  get keyMaterial () {
    return { cert: this.certificatePem, key: this.privateKeyPem }
  }

  /**
   * @returns {boolean} Does key material exist on the file system in PEM format?
   */
  get keyMaterialExists () {
    return (fs.existsSync(this.certificateFilePath) && fs.existsSync(this.privateKeyFilePath))
  }

  get privateKeyFileName () {
    return this.fileNameFor(this.privateKeyBaseFileName)
  }

  get certificateFileName () {
    return this.fileNameFor(this.certificateBaseFileName)
  }

  get privateKeyFilePath () {
    return this.filePathFor(this.privateKeyFileName)
  }

  get certificateFilePath () {
    return this.filePathFor(this.certificateFileName)
  }

  /**
   * Initialise the certificate either by loading the key material from the
   * file system or by creating it if it doesn’t already exist.
   *
   * @returns {void}
   */
  initialise() {
    if (this.keyMaterialExists) {
      this.loadKeyMaterial()
      return this.LOADED_KEY
    } else {
      this.createKeyMaterial()
      return this.CREATED_KEY
    }
  }

  /**
   * @param fileName {string} The file name (sans extension) to get the full file name for.
   * @returns {string} File name with extension for use when saving to the setting path.
   */
  fileNameFor (baseFileName) {
    return `${baseFileName}.pem`
  }

  /**
   * @param fileName {string} The file name (sans extension) to get the path for.
   * @returns {string} The file path for the passed file name from the settings path.
   */
  filePathFor (fileName) {
    return path.join(this.settingsPath, fileName)
  }

  signCertificate (privateKey) {
    this.certificate.sign(privateKey, forge.md.sha512.create())
  }
  
  /**
   * Loads persisted certificate and private key. Checks certificate is still valid
   * and, if not, asks for a new one to be created.
   */
  loadKeyMaterial () {
    const certificatePem = fs.readFileSync(this.certificateFilePath, 'utf-8')
    const certificate = forge.pki.certificateFromPem(certificatePem)

    // Check if the certificate is still valid and create a new one if not.
    if ((new Date()).getTime() >= certificate.validity.notAfter.getTime()) {
      events.emit(events.information.RENEWING, 'Localhost development certificate has expired. Renewing…')
      return this.createKeyMaterial()
    }
    
    this.certificatePem = certificatePem
    this.certificate = certificate
    this.privateKeyPem = fs.readFileSync(this.privateKeyFilePath, 'utf-8')
    this.privateKey = forge.pki.privateKeyFromPem(this.privateKeyPem)
  }

  /**
   * Starts creating the key material for this certificate. The process
   * ends in the specialised methods of concrete certificate instances
   * that extend this base class.
   *
   * @returns {void}
   */
  createKeyMaterial () {
    this.startCreatingKeyMaterial()
    this.finishCreatingKeyMaterial()
  }

  startCreatingKeyMaterial () {
    this.certificate = forge.pki.createCertificate()

    const keys = forge.pki.rsa.generateKeyPair(2048)
    this.certificate.publicKey = keys.publicKey
    this.privateKey = keys.privateKey

    // Serial number.
    // https://www.rfc-editor.org/rfc/rfc5280#section-4.1.2.2i
    // Note: This will fail after September 13, 275760.
    // The maximum value possible is 8639999996400000,
    // which is under the 20 octet limit mentioned in the RFC.
    this.certificate.serialNumber = new Date().getTime().toString()

    // Validity.
    this.certificate.validity.notBefore = new Date()
    this.certificate.validity.notAfter = new Date()
  }

  /**
    * Abstract method. Implement in sub-class.
    * (I could have just used a single method for this and super() but this
    * way the intent is much clearer.)
    */
  finishCreatingKeyMaterial () {
    throw new Error('finishCreatingKeyMaterial abstract method not implemented in subclass.')
  }

  /**
   * @param years {Number} Number of years certificate is valid for.
   */
  set validity (years) {
    this.certificate.validity.notAfter.setYear(this.certificate.validity.notAfter.getFullYear() + years)
  }

  persistKeyMaterial () {
    this.certificatePem = forge.pki.certificateToPem(this.certificate)
    this.privateKeyPem = forge.pki.privateKeyToPem(this.privateKey)

    fs.writeFileSync(this.certificateFilePath, this.certificatePem, 'utf-8')
    fs.writeFileSync(this.privateKeyFilePath, this.privateKeyPem, 'utf-8')
  }
}
