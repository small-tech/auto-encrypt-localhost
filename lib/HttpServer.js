////////////////////////////////////////////////////////////////////////////////
//
// HttpServer
//
// (Singleton; please use HttpServer.getSharedInstance() to access.)
//
// A simple HTTP server that:
//
//   1. Forwards http requests to https requests using a 307 redirect.
//   2. Serves the local root certificate authority public key at /.ca
//
// Copyright © 2020-present Aral Balkan, Small Technology Foundation.
// License: AGPLv3.
//
////////////////////////////////////////////////////////////////////////////////

import http from 'http'
import encodeUrl from 'encodeurl'

import events from './Events.js'

export default class HttpServer {
  //
  // Singleton access (async).
  //
  static instance = null
  static isBeingInstantiatedViaSingletonFactoryMethod = false

  static async getSharedInstance (certificateAuthority) {
    if (HttpServer.instance === null) {
      if (certificateAuthority == undefined) {
        throw new Error('Missing argument: certificateAuthority')
      }

      HttpServer.isBeingInstantiatedViaSingletonFactoryMethod = true
      HttpServer.instance = new HttpServer(certificateAuthority)
      await HttpServer.instance.init()
    }
    return HttpServer.instance
  }

  static async destroySharedInstance () {
    if (HttpServer.instance === null) {
      events.emit(events.information.HTTP_SERVER_NOTHING_TO_DESTROY, 'HTTP Server was never setup. Nothing to destroy.')
      return
    }

    events.emit(events.information.HTTP_SERVER_DESTROYING_SERVER, 'Destroying HTTP Server…')
    await HttpServer.instance.destroy()
    HttpServer.instance = null
    events.emit(events.information.HTTP_SERVER_DESTROYED, 'HTTP Server is destroyed.')
  }

  //
  // Private.
  //

  constructor (certificateAuthority) {
    // Ensure singleton access.
    if (HttpServer.isBeingInstantiatedViaSingletonFactoryMethod === false) {
      throw new Error('HttpServer is a singleton. Please instantiate using the HttpServer.getSharedInstance() method.')
    }
    HttpServer.isBeingInstantiatedViaSingletonFactoryMethod = false

    this.certificateAuthority = certificateAuthority

    this.server = http.createServer((request, response) => {
      if (request.url === '/.ca') {
        // Serve certificate authority certificate at /.ca.
        events.emit(events.information.HTTP_SERVER_SERVING_CA_CERTIFICATE, 'Serving local root Certificate Authority (CA) certificate at /.ca')
        response.writeHead(
          200,
          {
            'Content-Type': 'application/x-pem-file',
            'Content-Disposition': `attachment; filename="${this.certificateAuthority.certificateFileNameWithSerialNumber}"`
          }
        )
        response.end(this.certificateAuthority.certificatePem)
      } else {
        // Act as HTTP-to-HTTPS forwarder.
        // 
        // This means servers using Auto Encrypt Localhost will get automatic
        // HTTP to HTTPS forwarding and will not fail if they are accessed over HTTP.
        // (This behaves the same was as public servers created using Auto Encrypt.)
        let httpsUrl = null
        try {
          httpsUrl = new URL(`https://${request.headers.host}${request.url}`)
        } catch (error) {
          events.emit(events.errors.HTTP_SERVER_HTTPS_REDIRECT_FAILED, `Failed to redirect HTTP request: ${error}`)
          response.statusCode = 403
          response.end('403: forbidden')
          return
        }

        // Redirect HTTP to HTTPS.
        events.emit(events.information.HTTP_SERVER_REDIRECTING_TO_HTTPS, 'Redirecting HTTP request to HTTPS.')
        response.statusCode = 307
        response.setHeader('Location', encodeUrl(httpsUrl))
        response.end()
      }
    })
  }

  async init () {
    // The server is created on Port 80. On Linux, ensure the Node.js process has
    // the correct privileges for this to work. Looking forward to removing this notice
    // once Linux leaves the world of 1960s mainframe computers and catches up to other
    // prominent operating systems that don’t have this archaic restriction which, today,
    // is security theatre at best and a security vulnerability at worst.
    await new Promise((resolve, reject) => {

      this.server.on('error', error => {
        if (error.code === 'EADDRINUSE') {
          events.emit(events.warnings.HTTP_SERVER_PORT_80_BUSY, 'Port 80 is busy; skipping http redirection server for this instance.')
          resolve()
          return
        }
        reject(error)
      })

      this.server.listen(80, (error) => {
        if (error) reject(error)

        events.emit(events.information.HTTP_SERVER_LISTENING, 'HTTP server is listening on port 80.')
        resolve()
      })
    })
  }

  async destroy () {
    // Kills all connections and closes the server.
    this.server.closeAllConnections()
    this.server.close()

    // Wait until the server is closed before returning.
    await new Promise((resolve, reject) => {
      this.server.on('close', () => {
        resolve()
      })
      this.server.on('error', (error) => {
        reject(error)
      })
    })
  }
}
