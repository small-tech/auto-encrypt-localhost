import EventEmitter from 'node:events'

class Events extends EventEmitter {
  // Event name constants.
  INFORMATION = 'information'
  WARNINGS = 'warnings'
  ERRORS = 'errors'
  
  information = {
    RENEWING: Symbol(),

    HTTP_SERVER_NOTHING_TO_DESTROY: Symbol(),
    HTTP_SERVER_DESTROYING_SERVER: Symbol(),
    HTTP_SERVER_DESTROYED: Symbol(),

    HTTP_SERVER_LISTENING: Symbol(),
    HTTP_SERVER_SERVING_CA_CERTIFICATE: Symbol(),
    HTTP_SERVER_REDIRECTING_TO_HTTPS: Symbol()
  }

  warnings = {
    UNSUPPORTED_LINUX_SYSTEM_STORE: Symbol(),
    FIREFOX_POLICY_FILE_ALREADY_EXISTS: Symbol(),
    FIREFOX_NOT_FOUND_IN_DEFAULT_LOCATION: Symbol(),

    HTTP_SERVER_PORT_80_BUSY: Symbol()
  }

  errors = {
    CREATING_FIREFOX_POLICY_FILE: Symbol(),

    HTTP_SERVER_HTTPS_REDIRECT_FAILED: Symbol()
  }

  onAll (eventType, eventHandler) {
    Object.values(this[eventType]).forEach(event => this.on(event, eventHandler))
  }
}

const events = new Events()
export default events
