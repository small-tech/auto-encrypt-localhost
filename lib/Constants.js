import os from 'node:os'
import path from 'node:path'

export const DATA_HOME = path.join(
  process.env.XDG_DATA_HOME || process.env.HOME || os.homedir(),
  '.local',
  'share'
)

export const SMALL_TECH_HOME_PATH = path.join(DATA_HOME, 'small-tech.org')
export const DEFAULT_SETTINGS_PATH = path.join(SMALL_TECH_HOME_PATH, 'auto-encrypt-localhost')
export const TEST_SETTINGS_PATH = path.join(SMALL_TECH_HOME_PATH, 'auto-encrypt-localhost-test')

