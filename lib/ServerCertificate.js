/**
 * TLS 1.3 (RFC 8446) Server Certificate.
 * https://www.rfc-editor.org/rfc/rfc8446#section-4.4.2.2
 *
 * @copyright © 2019-present Aral Balkan, Small Technology Foundation.
 * @license AGPLv3.
 */

import os from 'node:os'
import AbstractCertificate from './AbstractCertificate.js'

export default class ServerCertificate extends AbstractCertificate {
  constructor (settingsPath, certificateAuthority) {
    super(settingsPath)

    if (certificateAuthority == undefined) {
      throw new Error('Missing argument: certificateAuthority')
    }

    this.certificateAuthority = certificateAuthority

    this.privateKeyBaseFileName = 'auto-encrypt-localhost-key'
    this.certificateBaseFileName = 'auto-encrypt-localhost'

    this.initialise()
  }

  get keyMaterial () {
    // Add the certificate authority so it doesn’t have to be
    // read in and recreated again if it’s needed anywhere else
    // (e.g., for serving the CA certificate from the HTTP
    // server so it can be added to local testing devices.)
    return Object.assign(super.keyMaterial, {
      certificateAuthority: this.certificateAuthority
    })
  }

  finishCreatingKeyMaterial () {
    this.validity = 1 // year

    this.certificate.setIssuer(this.certificateAuthority.certificate.subject.attributes)

    this.certificate.setSubject([
      {
        shortName: 'CN',
        value: `Localhost certificate for ${this.accountAtHost}`
      }
    ])

    // Support all local interfaces so that the machine can be reached over the 
    // local network via IPv4. This is useful for testing with multiple devices
    // over the local area network without needing to expose the machine over
    // the wide area network/Internet using a service like pageKite or ngrok
    // (or via a VPN using Wireguard and network forwarding rules).
    //
    // Note: unlike the hostname, ip addresses use the ip key in Node Forge,
    // not the value key. (See https://github.com/digitalbazaar/forge/issues/714)

    const localIPv4AddressAltNames = Object.entries(os.networkInterfaces())
    .map(iface => iface[1]
      .filter(addresses => addresses.family === 'IPv4')
      .map(addresses => { return { type: this.IP_TYPE, ip: addresses.address } })
    )
    .flat()

    // Also add support for local IP addresses that map to 127.0.0.1/localhost
    // (127.0.0.2 - 127.0.0.4) for testing peer-to-peer Small Web sites/apps.
    // See: https://codeberg.org/small-tech/auto-encrypt-localhost/issues/6
    const additionalLocalIPv4AddressAltNames = [2,3,4].map(subnet => {
      return {
        type: this.IP_TYPE,
        ip: `127.0.0.${subnet}`
      }
    })

    // Also add support for four local subdomains that can be used for testing
    // peer-to-peer Small Web sites/apps.
    // See: https://codeberg.org/small-tech/auto-encrypt-localhost/issues/6
    const localSubdomainAltNames = ['', 'place1.', 'place2.', 'place3.', 'place4.']
      .map(subdomain => {
        return {
          type: this.DNS_TYPE,
          value: `${subdomain}localhost`
        }
      })
    
    const altNames = localSubdomainAltNames
      .concat(localIPv4AddressAltNames)
      .concat(additionalLocalIPv4AddressAltNames)

    this.certificate.setExtensions([
      {
        // Basic Constraints.
        // https://www.rfc-editor.org/rfc/rfc5280#section-4.2.1.9
        // State that we’re not a certificate authority.
        name: 'basicConstraints',
        cA: false
      },
      {
        // Subject Key Identifier.
        // https://www.rfc-editor.org/rfc/rfc5280#section-4.2.1.2
        name: 'subjectKeyIdentifier'
      },
      {
        // Authority Key Identifier.
        // https://www.rfc-editor.org/rfc/rfc5280#section-4.2.1.1
        name: 'authorityKeyIdentifier',
        authorityCertIssuer: true,
        serialNumber: this.certificateAuthority.certificate.serialNumber
      }, 
      {
        // Key Usage.
        // https://www.rfc-editor.org/rfc/rfc5280#section-4.2.1.3
        name: 'keyUsage',
        digitalSignature: true,
        keyEncipherment: true,
      }, 
      {
        // Extended Key Usage.
        // https://www.rfc-editor.org/rfc/rfc5280#section-4.2.1.12
        // Flag that this certificate is to be used for TLS WWW server authentication only.
        name: 'extKeyUsage',
        serverAuth: true
      },
      {
        // Subject alternative name.
        // https://www.rfc-editor.org/rfc/rfc5280#section-4.2.1.6
        name: 'subjectAltName',
        altNames
      }
    ])

    this.signCertificate(this.certificateAuthority.privateKey)
    this.persistKeyMaterial()
  }
}
