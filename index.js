/**
 * Automatically provisions and installs locally-trusted TLS certificates for Node.js
 * https servers (including Express.js, etc.).
 *
 * @module @small-tech/auto-encrypt-localhost
 * @copyright © 2019-present Aral Balkan, Small Technology Foundation.
 * @license AGPLv3.
 */

import os from 'node:os'
import https from 'node:https'

import { DEFAULT_SETTINGS_PATH } from './lib/Constants.js'
import HttpServer from './lib/HttpServer.js'
import { getKeyMaterial } from './lib/TLS.js'
import events from './lib/Events.js'

// Ensure we only run on supported platforms (fail early).
const platform = os.platform()
if (platform !== 'linux' && platform !== 'darwin' && platform !== 'win32') {
  throw new Error(
    `Sorry, Auto Encrypt Localhost is not supported on ${platform}.
    (Supported platforms are Linux, macOS, and Windows.)
  
    If you want to contribute support, please open an issue at:
    https://codeberg.org/small-tech/auto-encrypt-localhost

    Thank you.`
  )
}

/**
 * Auto Encrypt Localhost is a static class. Please do not instantiate.
 *
 * Use: AutoEncryptLocalhost.https.createServer(…)
 *
 * @alias module:@small-tech/auto-encrypt-localhost
 */
export default class AutoEncryptLocalhost {
  /**
   * By aliasing the https property to the AutoEncryptLocalhost static class itself, we enable
   * people to add AutoEncryptLocalhost to their existing apps by importing the module
   * and prefixing their https.createServer(…) line with AutoEncryptLocalhost:
   *
   * @example import AutoEncryptLocalhost from '@small-tech/auto-encrypt-localhost'
   * const server = AutoEncryptLocalhost.https.createServer()
   *
   * @static
   */
  static get https () { return AutoEncryptLocalhost }

  static events = events
  
  static getKeyMaterial (settingsPath) {
    return getKeyMaterial(this.settingsPathWithDefaultFallback(settingsPath))
  }

  static settingsPathWithDefaultFallback (settingsPath) {
    return settingsPath || DEFAULT_SETTINGS_PATH
  }
  
  /**
   * Automatically provisions trusted development-time (localhost) certificates in Node.js.
   *
   * @static
   * @param {Object}   [options]               Optional HTTPS options object with optional additional
   *                                           Auto Encrypt-specific configuration settings.
   * @param {String}   [options.settingsPath=<DATA HOME>/small-tech.org/auto-encrypt-localhost]
   *                                           Custom path to save the certificate and private key to.
   */
  static createServer(_options, _listener) {
    // The first parameter is optional. If omitted, the first argument, if any, is treated as the request listener.
    if (typeof _options === 'function') {
      _listener = _options
      _options = {}
    }

    const options = _options || {}
    const listener = _listener || null

    const settingsPath = this.settingsPathWithDefaultFallback(options.settingsPath)

    // Get the key material (cert and key) and add them to the options
    // object. If a certificate authority and certificate needs to be
    // created or renewed, that will all be handled for us.
    const keyMaterial = getKeyMaterial(settingsPath)
    Object.assign(options, {cert: keyMaterial.cert, key: keyMaterial.key})

    //
    // Monkey-patch server.
    //

    /**
      Auto Encrypt Localhost’s patched (async) version of Node’s built-in `https.listen()` method.

      You can still use this method with exactly the same signature as Node’s and
      with a callback but, ideally, you should be awaiting it and not using the callback.
    
      @param {number|function} [port]
      @param {string|function} [host]
      @param {number|function} [backlog]
      @param {() => void} [callback]
    */
    const patchedListen = async function(port, host, backlog, callback) {
      // Start HTTP server.
      await HttpServer.getSharedInstance(keyMaterial.certificateAuthority)

      // Start HTTPS server.

      // We need to support the messy old-school overloaded method signature
      // to remain backwards compatible with existing implementations that will be
      // using the callback approach.
      //
      // server.listen([port[, host[, backlog]]][, callback])
      //
      // (See https://nodejs.org/api/net.html#serverlistenport-host-backlog-callback)
      let originalCallback = () => {}
      return await new Promise((resolve, reject) => {
        const args = []
        if (typeof port === 'number') {
          args.push(port)
          if (typeof host === 'string') {
            args.push(host)
            if (typeof backlog === 'number') {
              args.push(backlog)
            } else {
              if (typeof backlog === 'function') {
                originalCallback = backlog
              }
            }
          } else {
            if (typeof host === 'function') {
              originalCallback = host
            }
          }
        } else {
          originalCallback = port
        }
        const callback = error => {
          if (error) {
            reject(error)
            return
          }
          originalCallback()
          setImmediate(resolve)
        }
        args.push(callback)
        this.__autoEncryptLocalhost__originalListen.apply(this, args)
      })
    }

    /**
      Auto Encrypt Localhost’s patched (async) version of Node’s built-in `https.close()` method.

      Unlike Node’s `close()` method, this version also calls `closeAllConnections()` so it
      will sever any existing idle and active connections. When `await`ed, this call will
      only return once `node:https`’s close callback is called. This is also where your
      callback will fire, if you are using Node’s original method signature will callbacks.

      You can still use this method with exactly the same signature as Node’s and
      with a callback but, ideally, you should be awaiting it and not using the callback.
    
      @param {() => void} [callback]
    */
    const patchedClose = async function (callback) {
      // Shut down HTTP redirection server.
      await HttpServer.destroySharedInstance()

      // Shut down HTTPS server.
      await new Promise((resolve, _reject) => {
        this.__autoEncryptLocalhost__originalClose(() => {
          if (typeof callback === 'function') {
            callback()
          }
          resolve()
        })

        // Sever all idle and active connections.
        server.closeAllConnections()
      })
    }

    // Create HTTPS server.
    const server = /** @type {{ listen: patchedListen, close: patchedClose } & https.Server} */ (https.createServer(options, listener))

    server.__autoEncryptLocalhost__self = this

    // Monkey-patch server’s listen method so we can start up the HTTP
    // redirection server at the same time.
    // This also means that the listen() method is now async.
    server.__autoEncryptLocalhost__originalListen = server.listen
    server.listen = patchedListen

    // Monkey-patch server’s close method so we can perform clean-up and
    // shut down HTTP redirection server transparently when server.close() is called.
    // This also means the close() method is now async.
    server.__autoEncryptLocalhost__originalClose = server.close.bind(server)
    server.close = patchedClose

    return server
  }
}
