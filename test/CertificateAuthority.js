import fs from 'node:fs'
import test from 'tape'

import forge from 'node-forge'
import { TEST_SETTINGS_PATH } from '../lib/Constants.js'
import CertificateAuthority from '../lib/CertificateAuthority.js'

test('CertificateAuthority', t => {
  t.plan(5)
  const certificateAuthority = new CertificateAuthority(TEST_SETTINGS_PATH)

  // Ensure all files exist where they should.
  t.true(fs.existsSync(certificateAuthority.certificateFilePath), 'certificate file exists')
  t.true(fs.existsSync(certificateAuthority.privateKeyFilePath), 'private key file exists')
  t.true(fs.existsSync(certificateAuthority.certificatePathForSystemTrustStore), 'certificate exists in correct location for seeding system trust store')
  t.true(fs.existsSync(certificateAuthority.firefoxPoliciesFilePath), 'firefox policy file exists in correct location')

  // Ensure key material are written correctly and can be loaded and deserialised properly.
  t.doesNotThrow(
    _ => {
      forge.pki.certificateFromPem(fs.readFileSync(certificateAuthority.certificateFilePath, 'utf-8'))
      forge.pki.privateKeyFromPem(fs.readFileSync(certificateAuthority.privateKeyFilePath, 'utf-8'))
    },
    'deserialising certificate and private key does not throw.'
  )

  t.end()
})
