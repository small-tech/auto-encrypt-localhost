import fs from 'node:fs'
import test from 'tape'

import forge from 'node-forge'

import { TEST_SETTINGS_PATH } from '../lib/Constants.js'
import CertificateAuthority from '../lib/CertificateAuthority.js'
import ServerCertificate from '../lib/ServerCertificate.js'

test('ServerCertificate', t => {
  t.plan(5)
  t.throws(
    _ => new ServerCertificate(TEST_SETTINGS_PATH),
    /Missing argument: certificateAuthority/,
    'missing certificate authority throws'
  )

  const certificateAuthority = new CertificateAuthority(TEST_SETTINGS_PATH)
  const serverCertificate = new ServerCertificate(TEST_SETTINGS_PATH, certificateAuthority)

  // Ensure certificate is created with expected base alt names.

  // Note: these are the alt names we expect in every certificate. There will be
  // external interface IPs that will change depending on the network the machine
  // running the tests is on. We’re currently not testing for this.
  const expectedAltNames = new Set([
    { type: 2, value: 'localhost' },
    { type: 2, value: 'place1.localhost' },
    { type: 2, value: 'place2.localhost' },
    { type: 2, value: 'place3.localhost' },
    { type: 2, value: 'place4.localhost' },
    { type: 7, ip: '127.0.0.1' },
    { type: 7, ip: '127.0.0.2' },
    { type: 7, ip: '127.0.0.3' },
    { type: 7, ip: '127.0.0.4' }
  ])

  const actualAltNames = new Set(
    serverCertificate.certificate.getExtension('subjectAltName').altNames
  )

  actualAltNames.forEach(actualAltName => {
    expectedAltNames.forEach(expectedAltName => {
      if (actualAltName.type === expectedAltName.type && actualAltName.value === expectedAltName.value) {
        expectedAltNames.delete(expectedAltName)
      }
    })
  })

  t.strictEquals(expectedAltNames.size, 0, 'All expected base alt names exist')

  // Ensure all files exist where they should.
  t.true(fs.existsSync(serverCertificate.certificateFilePath), 'certificate file exists')
  t.true(fs.existsSync(serverCertificate.privateKeyFilePath), 'private key file exists')

  // Ensure key material are written correctly and can be loaded and deserialised properly.
  t.doesNotThrow(
    _ => {
      forge.pki.certificateFromPem(fs.readFileSync(serverCertificate.certificateFilePath, 'utf-8'))
      forge.pki.privateKeyFromPem(fs.readFileSync(serverCertificate.privateKeyFilePath, 'utf-8'))
    },
    'deserialising certificate and private key does not throw.'
  )

  t.end()
})
