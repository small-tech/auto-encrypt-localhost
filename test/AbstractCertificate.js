import fs from 'node:fs'
import path from 'node:path'
import test from 'tape'

import AbstractCertificate from '../lib/AbstractCertificate.js'
import { DATA_HOME } from '../lib/Constants.js'

test ('AbstractCertificate', t => {
  t.plan(4)
  t.throws(_ => {
    new AbstractCertificate()
  }, /Missing argument/, 'missing settings path throws')

  const settingsPath = path.join(DATA_HOME, 'small-tech.org', 'auto-encrypt-localhost-test')

  // Ensure we start with a non-existent settings path.
  fs.rmSync(settingsPath, {recursive: true, force: true})

  const abstractCertificate = new AbstractCertificate(settingsPath)

  t.strictEquals(abstractCertificate.settingsPath, settingsPath, 'settings path is set correctly')
  t.true(fs.existsSync(settingsPath), 'settings path is created')

  t.throws(
    _ => abstractCertificate.initialise(),
    /finishCreatingKeyMaterial abstract method not implemented in subclass/,
    'attempting to initialise abstract certificate instance throws'
  )

  t.end()
})
