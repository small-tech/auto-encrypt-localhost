import os from 'node:os'
import fs from 'node:fs'

import test from 'tape'

import { DEFAULT_SETTINGS_PATH } from '../lib/Constants.js'

import AutoEncryptLocalhost from '../index.js'
import { getKeyMaterial } from '../lib/TLS.js'

const downloadString = async url => await (await fetch(url)).text()
const downloadBuffer = async url => Buffer.from(await (await fetch(url)).arrayBuffer())

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

test('certificate creation', async t => {
  // The number of local IP addresses affects the number of tests
  // we will have so let’s calculate them now.
  const localIPv4Addresses =
    Object.entries(os.networkInterfaces())
    .map(iface =>
      iface[1].filter(addresses =>
        addresses.family === 'IPv4')
        .map(addresses => addresses.address)).flat()

  t.plan(6 + localIPv4Addresses.length)
  const settingsPath = DEFAULT_SETTINGS_PATH

  // Ensure we start with a non-existent settings path.
  fs.rmSync(settingsPath, {recursive: true, force: true})

  // Run Auto Encrypt Localhost.
  let server
  t.doesNotThrow(
    _ => {
      server = AutoEncryptLocalhost.https.createServer((_request, response) => {
        response.end('ok')
      })
    },
    'Creating server does not throw.'
  )

  try {
    await server.listen(443, () => t.pass('Old-style callback to listen gets called'))
  } catch (error) {
      t.fail(`Error on server listen: ${error}`)
  }
  t.pass('Can listen on server')

  const response = await downloadString('https://localhost')

  t.strictEquals(response, 'ok', 'Response from server is as expected for access via localhost.')

  // Test access from all local interfaces with IPv4 addresses.
  await asyncForEach(localIPv4Addresses, async localIPv4Address => {
    const response = await downloadString(`https://${localIPv4Address}`)
    t.strictEquals(response, 'ok', `Response from server is as expected for access via ${localIPv4Address}`)
  })

  // Test downloading the local root certificate authority public key via /.ca route.
  const keyMaterial = getKeyMaterial(DEFAULT_SETTINGS_PATH)
  const downloadedRootCABuffer = await downloadBuffer('http://localhost/.ca')
  const localRootCABuffer = Buffer.from(keyMaterial.certificateAuthority.certificatePem)
 
  t.strictEquals(Buffer.compare(localRootCABuffer, downloadedRootCABuffer), 0, 'The local root certificate authority public key is served correctly.')

  // Wait the for the first server to close.
  await server.close(() => t.pass('Old-style callback to close gets called'))

  t.end()
})


test ('multiple servers', async t => {
  t.plan(2)
  const server1Response = 'Server 1'
  const server2Response = 'Server 2'

  const server1 = AutoEncryptLocalhost.https.createServer((_request, response) => { response.end(server1Response) })
  await server1.listen(443)

  const server2 = AutoEncryptLocalhost.https.createServer((_request, response) => { response.end(server2Response) })
  await server2.listen(444)

  const result1 = await downloadString('https://localhost')
  const result2 = await downloadString('https://localhost:444')

  t.strictEquals(result1, server1Response, 'Server 1 response is as expected.')
  t.strictEquals(result2, server2Response, 'Server 2 response is as expected.')

  await server1.close()
  await server2.close()

  t.end()
})
