import test from 'tape'
import forge from 'node-forge'

import { getKeyMaterial } from '../lib/TLS.js'
import { TEST_SETTINGS_PATH } from '../lib/Constants.js'

test('TLS', t => {
  t.plan(2)
  const keyMaterial = getKeyMaterial(TEST_SETTINGS_PATH)

  t.true(keyMaterial.cert.match(/-----BEGIN CERTIFICATE-----[\s\S]+?-----END CERTIFICATE-----/m), 'shape of cert is correct')
  t.true(keyMaterial.key.match(/-----BEGIN RSA PRIVATE KEY-----[\s\S]+?-----END RSA PRIVATE KEY-----/m), 'shape of key is correct')
  
  t.end()
})
