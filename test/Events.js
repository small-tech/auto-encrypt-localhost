import test from 'tape'

import events from '../lib/Events.js'
import EventEmitter from 'node:events'

test('Events', async t => {
  t.plan(3)
  t.true(events instanceof EventEmitter, 'Events should be an EventEmitter')

  await new Promise((resolve, _reject) => {
    events.on('someEvent', eventObject => {
      t.pass('Event handler gets called.')
      t.strictEquals(eventObject.info, 'some info')
      resolve()
    })
    events.emit('someEvent', {info: 'some info'})
  })
  t.end()
})
