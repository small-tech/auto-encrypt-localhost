#!/usr/bin/env node

/**
 * Auto Encrypt Localhost: Command Line Interface (CLI)
 *
 * Copyright © 2022-present Aral Balkan, Small Technology Foundation.
 * Licensed under GNU AGPL version 3.0.
 */

import os from 'node:os'
import fs from 'node:fs'
import path from 'node:path'
import sade from 'sade'

import AutoEncryptLocalhost from '../index.js'
import { DEFAULT_SETTINGS_PATH } from '../lib/Constants.js'
import events from '../lib/Events.js'

import { fileURLToPath } from 'url'
const __dirname = fileURLToPath(new URL('.', import.meta.url))

const packageInfo = JSON.parse(fs.readFileSync(path.resolve(__dirname, '..', 'package.json')))

class CommandLineInterface {
  constructor () {
    console.info(`Auto Encrypt Localhost version ${packageInfo.version}`)

    this.handleEvents()
    
    this.commands = sade('auto-encrypt-localhost')
    
    // Overwrite the built-in _version() method called by the Sade version
    // command to format the output.
    this.commands._version = () => { 
      // Just exit, the version is printed with the app name on every run.
      this.printFooter()
      process.exit()
    }

    this.commands
      .version(packageInfo.version)
      .describe(`Creates locally-trusted TLS certificates.`)
  
    this.commands
      .command('create', '', {default: true})
      .describe('Create certificate authority and server certificate if necessary')
      .action(() => {
        AutoEncryptLocalhost.getKeyMaterial()
        console.info('\n🎉 Certificates created and added to local trust stores.\n')
        console.info('Find them at:')
        // Make the location clickable in Linux (this doesn’t seem to do anything
        // in macOS, at least on iTerm, and it messes up on Windows).
        const pathPrefix = os.platform === 'linux' ? 'file://' : ''
        console.info(pathPrefix + DEFAULT_SETTINGS_PATH)
        this.printFooter()
      })

    this.commands
      .command('serve')
      .describe('Run local server and serve certificate authority certificate at http://<local IPs>/.ca')
      .action(async () => {
        console.info()
        const server = AutoEncryptLocalhost.https.createServer((_request, response) => {
          const listOfLinks = Object.entries(os.networkInterfaces())
            .map(iface => iface[1].filter(addresses => addresses.family === 'IPv4')
            .map(addresses => addresses.address))
            .flat()
            .map(ipAddress => `<li><a href='http://${ipAddress}/.ca'>http://${ipAddress}/.ca</a></li>`)
            .join('')

          response.end(`
            <!DOCTYPE html>
            <html lang='en'>
              <head>
                <meta charset='UTF-8'>
                <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                <title>Auto Encrypt Localhost</title>
                <style>
                  body {
                    max-width: 50ch;
                    font-family: system-ui;
                    font-size: 1.5rem;
                    margin-left: auto;
                    margin-right: auto;
                    padding-left: 2em;
                    padding-right: 2em;
                  }
                </style>
              </head>
              <body>
                <h1>🔒 Auto Encrypt Localhost</h1>
                <p>You can download your local development Certificate Authority (CA) certificate from your local network from the following addresses:</p>
                <ul>
                  ${listOfLinks}
                </ul>
                <h2>Like this? Fund us!</h2>
                <p><a href="https://small-tech.org">Small Technology Foundation</a> is a tiny, independent not-for-profit.</p>
                <p>We exist in part thanks to patronage by people like you. If you share <a href="https://small-tech.org/about/#small-technology">our vision</a> and want to support our work, please <a href="https://small-tech.org/fund-us">become a patron or donate to us</a> today and help us continue to exist.</p>
              </body>
            </html>
          `)
        })

        await server.listen(443)

        console.info('\nHTTPS Server running. Please see https://localhost for instructions.')
        this.printFooter()
      })

    this.commands
      .command('show')
      .describe('Print out certificate authority and server certificate key material')
      .action(() => {
        const keyMaterial = AutoEncryptLocalhost.getKeyMaterial()
        console.info('\nLocal development certificate:\n')
        console.info(keyMaterial.cert)

        console.info('Local development certificate private key:\n')
        console.info(keyMaterial.key)

        console.info('Local development certificate authority (CA) certificate:\n')
        console.info(keyMaterial.certificateAuthority.certificatePem)

        console.info('Local development certificate authority (CA) private key:\n')
        console.info(keyMaterial.certificateAuthority.privateKeyPem)

        this.printFooter()
      })
    
    // Aliases for global options (to allow people to trigger them without using
    // double-dashes, for the sake of consistency).
    this.commands
      .command('help [subCommand]')
      .describe('Alias for --help, -h.')
      .action((subCommand = '') => this.commands.help(subCommand))

    this.commands
      .command('version')
      .describe('Alias for --version, -v.')
      .action(() => this.commands._version())
  }

  printFooter () {
    console.info()
    console.info('╔═══════════════════════════════════════════╗')
    console.info('║ Like this? Fund us!                       ║')
    console.info('║                                           ║')
    console.info('║ We’re a tiny, independent not-for-profit. ║')
    console.info('║ https://small-tech.org/fund-us            ║')
    console.info('╚═══════════════════════════════════════════╝')
  }

  handleEvents () {
    // Just log all the event messages.
    events.onAll(events.INFORMATION, console.info)
    events.onAll(events.WARNINGS, console.warn)
    events.onAll(events.ERRORS, console.error)
  }

  parse (args) {
    this.commands.parse(args)  
  }
}

const cli = new CommandLineInterface()
cli.parse(process.argv)
