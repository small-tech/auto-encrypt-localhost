# Developer Documentation

This documentation includes the implementation details of Auto Encrypt Localhost and is intended to aid you if you’re trying to improve, debug, or get a deeper understanding of it.

If you just want to use Auto Encrypt Localhost, please see the public interface, as documented in the [README](readme.md).

## Like this? Fund us!

[Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.

## Requirements

Auto Encrypt Localhost is supported on:

  - __Node:__ LTS (currently 18.2.0+).
  - __ECMAScript:__ [ES2019](https://node.green/#ES2019)

## Overview of relationships

![Dependency relationship diagram for Auto Correct](artefacts/dependency-graph.svg)

(Node modules have been collapsed for clarity.)

Generated using [dependency cruiser](https://github.com/sverweij/dependency-cruiser).

To run dependency cruiser, you will need to [install Graphviz](https://graphviz.org/download/).

## How it works in more detail

Auto Encrypt Localhost automatically and transparently creates development certificates for your Node.js https servers. It is cross platform, supports the system trust store, Node.js trust store, and Firefox, Safari, and Chrom(ium). It’s implemented entirely in JavaScript and does not require native binaries like mkcert or certutil.

Specifically, Auto Encrypt Localhost:

  - Creates a root Certificate Authority.

  - Creates locally-trusted TLS certificates valid for localhost, 127.0.0.1, ::1, and any other local IP addresses you might have.

You can use these certificates for local development without triggering self-signed certificate errors.

It’s very simple to use: you simply use `AutoEncryptLocalhost.https` to create your HTTPS servers instead of using the built-in Node.js `https` module.

## Testing

### Functional tests

To test that the generated certificates are valid:

1. Run the basic server example:
    ```shell
    node examples/server
    ```

2. In a separate terminal session, run the following [OpenSSL](https://www.openssl.org/) command:

  ```shell
  openssl s_client -connect localhost:443 -CAfile ~/.local/share/small-tech.org/auto-encrypt-localhost/auto-encrypt-localhost-CA.pem
  ```

    Your output should resemble the following:

    ```
    CONNECTED(00000003)
    Can't use SSL_get_servername
    depth=1 O = Auto Encrypt Localhost, OU = https://codeberg.org/small-tech/auto-encrypt-localhost, CN = Localhost Certificate Authority for aral at dev.ar.al
    verify return:1
    depth=0 CN = Localhost certificate for aral at dev.ar.al
    verify return:1
    ---
    Certificate chain
     0 s:CN = Localhost certificate for aral at dev.ar.al
       i:O = Auto Encrypt Localhost, OU = https://codeberg.org/small-tech/auto-encrypt-localhost, CN = Localhost Certificate Authority for aral at dev.ar.al
       a:PKEY: rsaEncryption, 2048 (bit); sigalg: RSA-SHA512
       v:NotBefore: Jan  6 21:29:38 2023 GMT; NotAfter: Jan  6 21:29:38 2024 GMT
    ---
    Server certificate
    -----BEGIN CERTIFICATE-----
    MIIEhDCCA2ygAwIBAgIHAWcwQFeJWTANBgkqhkiG9w0BAQ0FADCBojEfMB0GA1UE
    ChMWQXV0byBFbmNyeXB0IExvY2FsaG9zdDE/MD0GA1UECxM2aHR0cHM6Ly9jb2Rl
    YmVyZy5vcmcvc21hbGwtdGVjaC9hdXRvLWVuY3J5cHQtbG9jYWxob3N0MT4wPAYD
    VQQDEzVMb2NhbGhvc3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IGZvciBhcmFsIGF0
    IGRldi5hci5hbDAeFw0yMzAxMDYyMTI5MzhaFw0yNDAxMDYyMTI5MzhaMDYxNDAy
    BgNVBAMTK0xvY2FsaG9zdCBjZXJ0aWZpY2F0ZSBmb3IgYXJhbCBhdCBkZXYuYXIu
    YWwwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDZow3CZUk6phWjWrSo
    2kWMl/4fCxB2QXGywF1zu0VE48eOFCs8CzOMtPpNDeTomMy7k72n4F5xN6u+8U2U
    /4w25fL0AeF+j9Y7vvEMsm9tVIqHQGHoc0vVLWteMPXgPTkedBjDvVhlDFREFT/C
    qHil2NosskGEIvtK01uXvn9Ohn0P9m2nsiiCZ4Vq0eZUXy7xPynOCepBYKjHmG/D
    76jYS6/Bnj7z0bx2nvHlDY7/07C7cNGxyiflTvSg8grQFwOd4sxPVSuHBosGrTve
    Nilwq95FOyWnNuIhonyvov24mt+jPm9qDAQg0pXdrIfi3ScutqQ9JX5FgkQ2knaB
    eR07AgMBAAGjggEoMIIBJDAJBgNVHRMEAjAAMB0GA1UdDgQWBBSZICOuHPdL8p0y
    nn5kt7dz2tQfETCBvwYDVR0jBIG3MIG0oYGopIGlMIGiMR8wHQYDVQQKExZBdXRv
    IEVuY3J5cHQgTG9jYWxob3N0MT8wPQYDVQQLEzZodHRwczovL2NvZGViZXJnLm9y
    Zy9zbWFsbC10ZWNoL2F1dG8tZW5jcnlwdC1sb2NhbGhvc3QxPjA8BgNVBAMTNUxv
    Y2FsaG9zdCBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkgZm9yIGFyYWwgYXQgZGV2LmFy
    LmFsggcBZzBAV4dAMAsGA1UdDwQEAwIFoDATBgNVHSUEDDAKBggrBgEFBQcDATAU
    BgNVHREEDTALgglsb2NhbGhvc3QwDQYJKoZIhvcNAQENBQADggEBADW1ZRpoRWPi
    pb5GnCGWGmGPDtU4UJe387g1Gp2lVo1k3lMWKjS6MLy9W2v6i27XAEBTv9p1th0/
    kIPothBybjn9QMcLnUIvJ2VI7iokIHlOUVIBLCIdFaDFGdm1NFZW2O5N6zj3qqGy
    vNwrcGMIByG9qjmi+0Q+kP2zUWNaqIiFJfsJctjehoP1BdVJ64+ZUdWlPEyLl3YR
    I5Y3bIl2KuCB2gPatAdFjQKIDnY+g3N9EmlM5DQsnXSjGOYgVXaOcCGDuRgjUh1x
    RjCBTD8KKnsuZ9hNIKIkSBNuh4y50ClF9VN3fgD0Ug/xA5C3VUpdhXxA9kDivw0Y
    2u0SbPmdNZY=
    -----END CERTIFICATE-----
    subject=CN = Localhost certificate for aral at dev.ar.al
    issuer=O = Auto Encrypt Localhost, OU = https://codeberg.org/small-tech/auto-encrypt-localhost, CN = Localhost Certificate Authority for aral at dev.ar.al
    ---
    No client certificate CA names sent
    Peer signing digest: SHA256
    Peer signature type: RSA-PSS
    Server Temp Key: X25519, 253 bits
    ---
    SSL handshake has read 1716 bytes and written 375 bytes
    Verification: OK
    ---
    New, TLSv1.3, Cipher is TLS_AES_256_GCM_SHA384
    Server public key is 2048 bit
    Secure Renegotiation IS NOT supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    Early data was not sent
    Verify return code: 0 (ok)
    ---
    ---
    Post-Handshake New Session Ticket arrived:
    SSL-Session:
        Protocol  : TLSv1.3
        Cipher    : TLS_AES_256_GCM_SHA384
        Session-ID: CD60CEBBF24150CAFFF0C7E7F9061B5DBF0B959DC4CF90C146B2D29772EEA7AC
        Session-ID-ctx: 
        Resumption PSK: A1A93C0882ED95C92AFB8CB008D2945DDCB296DCAC7DFB89E455C12925A0089549854D2CAEA5C26FFA117167C06604E4
        PSK identity: None
        PSK identity hint: None
        SRP username: None
        TLS session ticket lifetime hint: 7200 (seconds)
        TLS session ticket:
        0000 - 6c eb 7f ab 6a cb 18 57-e9 3e b2 cc cc a3 ab 83   l...j..W.>......
        0010 - 6f 96 88 ec 84 36 48 7b-a9 34 e6 21 25 cf 72 06   o....6H{.4.!%.r.
        0020 - fe db c6 c6 16 5e 2b a0-7f 98 9c 53 53 13 d3 77   .....^+....SS..w
        0030 - ed 48 d6 1e c2 e1 dc 46-1b 9c 14 8c 06 b9 b6 57   .H.....F.......W
        0040 - ea 1e 10 e0 38 d6 65 c0-e2 8a 9b ed 19 34 c3 7f   ....8.e......4..
        0050 - d1 a8 c4 00 d7 3c d1 0a-7f c0 0f a9 c1 3f 06 55   .....<.......?.U
        0060 - f4 b3 f0 9f 6d db 4c 21-2b fa 6f a7 01 03 a4 81   ....m.L!+.o.....
        0070 - 74 30 8f 8b 76 7e e8 2a-60 55 9f 70 f2 ff 6f 3c   t0..v~.*`U.p..o<
        0080 - 28 11 a4 3b f2 90 f7 db-3c d5 be 19 3d f7 79 6a   (..;....<...=.yj
        0090 - 27 fa b4 fc a3 ed 72 ce-99 53 57 b4 86 fc 12 ac   '.....r..SW.....
        00a0 - 27 a1 43 7c 10 0b f0 7b-47 94 66 ec f2 07 53 7d   '.C|...{G.f...S}
        00b0 - 49 c5 f9 9f d0 dc 52 10-da ff 60 e0 b6 fa e4 bb   I.....R...`.....
        00c0 - 06 68 65 29 a9 41 a2 39-cf 22 f9 cc 1c 51 9a 9b   .he).A.9."...Q..
        00d0 - fa ac 05 d1 70 b9 6d 30-0f 1a 66 91 20 3f e9 04   ....p.m0..f. ?..
        00e0 - ca 4a 48 72 9d 77 79 a1-d8 b4 ce 8c 0b cf 5b c7   .JHr.wy.......[.

        Start Time: 1673517505
        Timeout   : 7200 (sec)
        Verify return code: 0 (ok)
        Extended master secret: no
        Max Early Data: 0
    ---
    read R BLOCK
    ---
    Post-Handshake New Session Ticket arrived:
    SSL-Session:
        Protocol  : TLSv1.3
        Cipher    : TLS_AES_256_GCM_SHA384
        Session-ID: C59E50585EDDA703A89856C3F4349CB371F711ACA7E9F811D3A86E28C91374E2
        Session-ID-ctx: 
        Resumption PSK: A0B6D6363EF2B7C6C26A92F2EDC9A738BB4117AF39DCC27FFEEDFD86F9ED75D0DB4371360DD6C334616E70C4E37F9A4C
        PSK identity: None
        PSK identity hint: None
        SRP username: None
        TLS session ticket lifetime hint: 7200 (seconds)
        TLS session ticket:
        0000 - 6c eb 7f ab 6a cb 18 57-e9 3e b2 cc cc a3 ab 83   l...j..W.>......
        0010 - 80 f5 d9 7c ba 1a 04 73-63 ea ad d5 53 d3 62 94   ...|...sc...S.b.
        0020 - f6 08 0c 4e 8d 76 9f c8-d4 ac 2e 34 a4 d3 c0 60   ...N.v.....4...`
        0030 - 90 01 f1 85 95 cf d8 30-57 b9 ca 18 13 c6 3e 7a   .......0W.....>z
        0040 - ef bb c1 1a 11 4c dc 7b-99 48 92 bb 79 b4 c6 4d   .....L.{.H..y..M
        0050 - 93 57 0a 48 c3 c3 c5 68-07 61 7d b1 3a c3 55 4c   .W.H...h.a}.:.UL
        0060 - c5 3c 8e 96 60 cd e6 c7-d6 63 94 15 77 6c 97 58   .<..`....c..wl.X
        0070 - b1 00 8e 48 64 ea 84 06-1b ea 58 24 39 a6 d9 eb   ...Hd.....X$9...
        0080 - c1 00 2e 86 7d 93 ab 76-96 8c 73 95 43 2a 20 fb   ....}..v..s.C* .
        0090 - dc 44 fd 59 6d d0 0d 35-70 9a 11 31 72 92 17 00   .D.Ym..5p..1r...
        00a0 - 3b d2 aa c2 ed b1 21 54-d0 1b 05 32 c9 3f ba 03   ;.....!T...2.?..
        00b0 - 1a 0e 14 79 74 a4 d7 db-27 f0 10 81 47 0e 46 a2   ...yt...'...G.F.
        00c0 - 11 ec 6b c1 77 32 54 46-ad c7 90 56 db 6f 3c 4e   ..k.w2TF...V.o<N
        00d0 - 43 91 73 b9 4b 7b e9 30-9d 27 d5 5a 47 b5 c0 ec   C.s.K{.0.'.ZG...
        00e0 - 02 54 57 e9 26 54 e4 15-16 87 08 13 75 13 1e 14   .TW.&T......u...

        Start Time: 1673517505
        Timeout   : 7200 (sec)
        Verify return code: 0 (ok)
        Extended master secret: no
        Max Early Data: 0
    ---
    read R BLOCK
    ```

3. Also test that you can successfully connect to https://localhost from:

    - The native browser of the platform (e.g., Safari on macOS, Edge on Windows).
    - Firefox.
    - Chromium-based browsers.

> 💡 GNOME Web does not appear to pick up certificate authorities when installed into the system trust store and it [does not have built-in support for certificate management at this time](https://help.gnome.org/users/epiphany/stable/cert.html.en) so it’s currently not supported. Pull requests welcome ;)

### Unit tests

#### On Linux and macOS:

```sh
npm -s test
```

To see debug output, run `npm -s run test-debug` instead.

Note that [npm cannot prompt for sudo passwords due to a bug](https://github.com/npm/cli/issues/2887) so you will be prompted for your password prior to the running of the tests in order to unlock sudo while the tests are running.

#### Windows (10 and 11):

```sh
npm -s run test-on-windows
```

> 💡 If you don’t want to answer yes to three security prompts, run your Windows Terminal session as administrator.

> 💡 On Windows, the test that ensures the Firefox policy file is created correctly fails the first time it’s run. The file is created correctly, I think this is because of some oddness with the forking behaviour on Node.js. Unless this translates to an actual issue during use, I’m happy to leave it be. I’ve spent enough time working around the quirks of that garbage fire of an operating system. Umm, what I mean to say is: pull requests welcome! ;)

## Coverage

#### On Linux and macOS:

```sh
npm -s run coverage
```

To see debug output, run `npm -s run coverage-debug` instead.

#### Windows (10 and 11):

```sh
npm -s run coverage-on-windows
```

## Documentation

To regenerate the dependency diagram and this documentation:

```sh
npm -s run generate-developer-documentation
```

<a name="module_@small-tech/auto-encrypt-localhost"></a>

## @small-tech/auto-encrypt-localhost
Automatically provisions and installs locally-trusted TLS certificates for Node.js
https servers (including Express.js, etc.).

**License**: AGPLv3.  
**Copyright**: © 2019-present Aral Balkan, Small Technology Foundation.  

* [@small-tech/auto-encrypt-localhost](#module_@small-tech/auto-encrypt-localhost)
    * [module.exports](#exp_module_@small-tech/auto-encrypt-localhost--module.exports) ⏏
        * [.https](#module_@small-tech/auto-encrypt-localhost--module.exports.https)
        * [.createServer([options])](#module_@small-tech/auto-encrypt-localhost--module.exports.createServer) ⇒ <code>https.Server</code>

<a name="exp_module_@small-tech/auto-encrypt-localhost--module.exports"></a>

### module.exports ⏏
Auto Encrypt Localhost is a static class. Please do not instantiate.

Use: AutoEncryptLocalhost.https.createServer(…)

**Kind**: Exported class  
<a name="module_@small-tech/auto-encrypt-localhost--module.exports.https"></a>

#### module.exports.https
By aliasing the https property to the AutoEncryptLocalhost static class itself, we enable
people to add AutoEncryptLocalhost to their existing apps by importing the module
and prefixing their https.createServer(…) line with AutoEncryptLocalhost:

**Kind**: static property of [<code>module.exports</code>](#exp_module_@small-tech/auto-encrypt-localhost--module.exports)  
**Example**  
```js
import AutoEncryptLocalhost from '@small-tech/auto-encrypt-localhost'
const server = AutoEncryptLocalhost.https.createServer()
```
<a name="module_@small-tech/auto-encrypt-localhost--module.exports.createServer"></a>

#### module.exports.createServer([options]) ⇒ <code>https.Server</code>
Automatically provisions trusted development-time (localhost) certificates in Node.js via mkcert.

**Kind**: static method of [<code>module.exports</code>](#exp_module_@small-tech/auto-encrypt-localhost--module.exports)  
**Returns**: <code>https.Server</code> - The server instance returned by Node’s https.createServer() method.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [options] | <code>Object</code> |  | Optional HTTPS options object with optional additional                                           Auto Encrypt-specific configuration settings. |
| [options.settingsPath] | <code>String</code> | <code>&lt;DATA HOME&gt;/small-tech.org/auto-encrypt-localhost</code> | Custom path to save the certificate and private key to. |


## Like this? Fund us!

[Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.

## Copyright

&copy; 2019-present [Aral Balkan](https://ar.al), [Small Technology Foundation](https://small-tech.org).

## License

[AGPL version 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
